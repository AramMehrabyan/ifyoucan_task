package com.example.ifyoucan_task.di

import com.example.ifyoucan_task.data.remote.api.WeatherApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import retrofit2.Retrofit

@Module
@InstallIn(ActivityRetainedComponent::class)
object ActivityRetainedModule {

    @Provides
    fun provideMoviesApi(retrofit: Retrofit): WeatherApi =
        retrofit.create(WeatherApi::class.java)
}