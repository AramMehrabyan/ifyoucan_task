package com.example.ifyoucan_task.di

import android.content.Context
import com.example.ifyoucan_task.data.local.CityDao
import com.example.ifyoucan_task.data.local.WeatherDataBase
import com.example.ifyoucan_task.data.remote.client.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideLoggingInterceptor(): HttpLoggingInterceptor =
        ApiService.getLoggingInterceptor()

    @Singleton
    @Provides
    fun provideHttpClient(logging: HttpLoggingInterceptor): OkHttpClient =
        ApiService.getHttpClient(logging)

    @Singleton
    @Provides
    fun provideConverterFactory(): GsonConverterFactory =
        ApiService.getConverterFactory()

    @Singleton
    @Provides
    fun provideRetrofit(
        converterFactory: GsonConverterFactory,
        client: OkHttpClient
    ): Retrofit = ApiService.getRetrofit(converterFactory, client)

    @Singleton
    @Provides
    fun provideWeatherDatabase(
        @ApplicationContext context: Context
    ): WeatherDataBase = WeatherDataBase.create(context)

    @Singleton
    @Provides
    fun provideCityDao(db: WeatherDataBase): CityDao = db.cityDao()
}