package com.example.ifyoucan_task.di

import com.example.ifyoucan_task.data.repository.WeatherRepository
import com.example.ifyoucan_task.data.repository.WeatherRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@Module
@InstallIn(ActivityRetainedComponent::class)
interface ActivityRetainedBindModule {

    @Binds
    fun bindMoviesRepository(
        analyticsServiceImpl: WeatherRepositoryImpl
    ): WeatherRepository
}