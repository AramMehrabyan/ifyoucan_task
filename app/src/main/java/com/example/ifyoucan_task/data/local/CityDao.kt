package com.example.ifyoucan_task.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.ifyoucan_task.data.model.City

@Dao
interface CityDao {

    @Query("SELECT * FROM city")
    suspend fun getLastSearchedCity(): List<City>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertLastSearchedCity(city: City)
}