package com.example.ifyoucan_task.data.remote.client

import com.example.ifyoucan_task.util.Constants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiService {

    fun getLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor()
    }

    fun getHttpClient(logging: HttpLoggingInterceptor): OkHttpClient {
        val builder = OkHttpClient.Builder().also {
            logging.level = HttpLoggingInterceptor.Level.HEADERS
            it.addInterceptor(logging)

            it.readTimeout(Constants.REQUEST_READ_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(Constants.REQUEST_CONNECTION_TIMEOUT, TimeUnit.SECONDS)
        }

        return builder.build()
    }

    fun getConverterFactory(): GsonConverterFactory = GsonConverterFactory.create()

    fun getRetrofit(
        converterFactory: GsonConverterFactory,
        client: OkHttpClient
    ): Retrofit {
        val builder = Retrofit.Builder()
            .baseUrl(Constants.WEATHER_API_URL)
            .addConverterFactory(converterFactory)
        builder.client(client)
        return builder.build()
    }
}