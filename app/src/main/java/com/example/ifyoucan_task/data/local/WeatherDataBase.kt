package com.example.ifyoucan_task.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.ifyoucan_task.data.model.City
import com.example.ifyoucan_task.util.Constants

@Database(entities = [City::class], version = 1, exportSchema = false)
abstract class WeatherDataBase : RoomDatabase() {

    abstract fun cityDao(): CityDao

    companion object {
        fun create(context: Context): WeatherDataBase =
            Room.databaseBuilder(
                context, WeatherDataBase::class.java, Constants.WEATHER_DB_NAME
            ).fallbackToDestructiveMigration().build()
    }
}