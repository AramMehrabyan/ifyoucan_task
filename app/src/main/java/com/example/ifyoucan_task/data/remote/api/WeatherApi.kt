package com.example.ifyoucan_task.data.remote.api

import com.example.ifyoucan_task.data.model.City
import com.example.ifyoucan_task.data.model.Weather
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    /**
     * remote request to get cities list
     *
     * @param searchKey: the city search key
     * @param limit: the response limit
     * @param apiKey: the weather api key
     * @return Response<List<City>>
     * @see City
     */
    @GET("/geo/1.0/direct")
    suspend fun getCitiesList(
        @Query("q") searchKey: String,
        @Query("limit") limit: Int,
        @Query("appid") apiKey: String
    ): Response<List<City>>

    /**
     * remote request to get city weather info
     *
     * @param name: the city name, state code, country code
     * @param units: the data type units
     * @param apiKey: the weather api key
     * @return Response<Weather>
     * @see Weather
     */
    @GET("/data/2.5/weather")
    suspend fun getCityWeatherInfo(
        @Query("q") name: String,
        @Query("units") units: String,
        @Query("appid") apiKey: String
    ): Response<Weather>
}