package com.example.ifyoucan_task.data.model

import com.google.gson.annotations.SerializedName

class Weather(
    @SerializedName("id")
    val id: Int,

    @SerializedName("name")
    val name: String?,

    @SerializedName("coord")
    val coordinate: Coordinate?,

    @SerializedName("weather")
    val weatherInfo: List<WeatherInfo>?,

    @SerializedName("base")
    val base: String?,

    @SerializedName("main")
    val mainInfo: MainInfo?,

    @SerializedName("visibility")
    val visibility: Float,

    @SerializedName("wind")
    val wind: Wind?,
)

class Coordinate(
    @SerializedName("lat")
    val latitude: Float,

    @SerializedName("lon")
    val longitude: Float
)

class WeatherInfo(
    @SerializedName("id")
    val id: Int,

    @SerializedName("main")
    val title: String?,

    @SerializedName("description")
    val description: String?,

    @SerializedName("icon")
    val icon: String?
)

class MainInfo(
    @SerializedName("temp")
    val temp: Float,

    @SerializedName("feels_like")
    val feelsLike: Float,

    @SerializedName("temp_min")
    val tempMin: Float,

    @SerializedName("temp_max")
    val tempMax: Float
)

class Wind(
    @SerializedName("speed")
    val speed: Float,

    @SerializedName("deg")
    val deg: Float,

    @SerializedName("gust")
    val gust: Float
)