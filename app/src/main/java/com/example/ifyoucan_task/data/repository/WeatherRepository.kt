package com.example.ifyoucan_task.data.repository

import com.example.ifyoucan_task.data.model.City
import com.example.ifyoucan_task.data.model.Weather
import retrofit2.Response

interface WeatherRepository {

    suspend fun getCitiesList(
        searchKey: String, limit: Int, apiKey: String
    ): Response<List<City>>

    suspend fun getCityWeatherInfo(
        name: String, units: String, apiKey: String
    ): Response<Weather>

    suspend fun insertLastSearchedCity(city: City)

    suspend fun getLastSearchedCity(): List<City>
}