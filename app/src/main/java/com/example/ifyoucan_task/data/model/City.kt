package com.example.ifyoucan_task.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.ifyoucan_task.util.Constants
import com.google.gson.annotations.SerializedName

@Entity(tableName = "city")
class City(

    @PrimaryKey
    val id: Int = Constants.LAST_SEARCHED_CITY_MODEL_ID,

    @SerializedName("name")
    val name: String?,

    @SerializedName("country")
    val country: String?,

    @SerializedName("state")
    val state: String?
) {

    fun getTitle(): String {
        val titleBuilder = StringBuilder(name ?: "")

        if (!state.isNullOrEmpty()) {
            titleBuilder.append(", $state")
        }

        if (!country.isNullOrEmpty()) {
            titleBuilder.append(", $country")
        }

        return titleBuilder.toString()
    }
}