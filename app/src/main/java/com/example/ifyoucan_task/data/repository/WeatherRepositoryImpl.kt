package com.example.ifyoucan_task.data.repository

import com.example.ifyoucan_task.data.local.CityDao
import com.example.ifyoucan_task.data.model.City
import com.example.ifyoucan_task.data.model.Weather
import com.example.ifyoucan_task.data.remote.api.WeatherApi
import retrofit2.Response
import javax.inject.Inject

class WeatherRepositoryImpl @Inject constructor(
    private val weatherApi: WeatherApi, private val cityDao: CityDao
) : WeatherRepository {

    override suspend fun getCitiesList(
        searchKey: String, limit: Int, apiKey: String
    ): Response<List<City>> = weatherApi.getCitiesList(searchKey, limit, apiKey)

    override suspend fun getCityWeatherInfo(
        name: String, units: String, apiKey: String
    ): Response<Weather> = weatherApi.getCityWeatherInfo(name, units, apiKey)

    override suspend fun insertLastSearchedCity(city: City) {
        cityDao.insertLastSearchedCity(city)
    }

    override suspend fun getLastSearchedCity(): List<City> = cityDao.getLastSearchedCity()
}