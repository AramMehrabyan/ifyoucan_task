package com.example.ifyoucan_task.data.remote.client

sealed class ResponseWrapper<out T> {

    class Success<out T>(val data: T) : ResponseWrapper<T>()

    class Error(val message: String) : ResponseWrapper<Nothing>()
}