package com.example.ifyoucan_task.domain

import com.example.ifyoucan_task.data.remote.client.ResponseWrapper
import com.example.ifyoucan_task.util.Constants
import retrofit2.Response

abstract class BaseUseCase {

    protected suspend fun <T> fetchData(
        call: suspend () -> Response<T>
    ): ResponseWrapper<T> {
        try {
            val response = call()

            if (response.isSuccessful) {
                val data = response.body()
                if (data != null) return ResponseWrapper.Success(data)
            }

            return error(response.message() ?: Constants.COMMON_ERROR_MESSAGE)
        } catch (e: Exception) {
            return error(e.message ?: Constants.COMMON_ERROR_MESSAGE)
        }
    }

    private fun <T> error(message: String): ResponseWrapper<T> {
        return ResponseWrapper.Error(message)
    }
}