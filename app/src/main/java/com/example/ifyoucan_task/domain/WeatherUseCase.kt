package com.example.ifyoucan_task.domain

import com.example.ifyoucan_task.BuildConfig
import com.example.ifyoucan_task.data.model.City
import com.example.ifyoucan_task.data.model.Weather
import com.example.ifyoucan_task.data.remote.client.ResponseWrapper
import com.example.ifyoucan_task.data.repository.WeatherRepository
import com.example.ifyoucan_task.util.Constants
import javax.inject.Inject

class WeatherUseCase @Inject constructor(
    private val weatherRepository: WeatherRepository
) : BaseUseCase() {

    suspend fun getCitiesList(
        searchKey: String, limit: Int = 10, apiKey: String = BuildConfig.API_KEY
    ): ResponseWrapper<List<City>> = fetchData {
        weatherRepository.getCitiesList(searchKey, limit, apiKey)
    }

    suspend fun getCityWeatherInfo(
        name: String, units: String = Constants.CELSIUS_UNIT,
        apiKey: String = BuildConfig.API_KEY
    ): ResponseWrapper<Weather> = fetchData {
        weatherRepository.getCityWeatherInfo(name, units, apiKey)
    }

    suspend fun insertLastSearchedCity(city: City) {
        weatherRepository.insertLastSearchedCity(city)
    }

    suspend fun getLastSearchedCity(): City? =
        weatherRepository.getLastSearchedCity().firstOrNull()
}