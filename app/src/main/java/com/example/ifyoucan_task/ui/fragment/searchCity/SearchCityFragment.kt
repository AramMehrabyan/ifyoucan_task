package com.example.ifyoucan_task.ui.fragment.searchCity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ifyoucan_task.R
import com.example.ifyoucan_task.data.model.City
import com.example.ifyoucan_task.databinding.FragmentSearchCityBinding
import com.example.ifyoucan_task.ui.base.view.BaseDataListFragment
import com.example.ifyoucan_task.ui.base.viewModel.BaseDataListViewModel
import com.example.ifyoucan_task.ui.fragment.searchCity.adapter.CitiesAdapter
import com.example.ifyoucan_task.util.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.FlowPreview

@FlowPreview
@AndroidEntryPoint
class SearchCityFragment : BaseDataListFragment<City>() {

    private val viewModel: SearchCityViewModel by viewModels()
    private lateinit var binding: FragmentSearchCityBinding

    private val adapter: CitiesAdapter by lazy {
        CitiesAdapter { model ->
            clickToListItem(model)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchCityBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        initSearchView()
        initRecyclerView()

        return binding.root
    }

    private fun initSearchView() {
        binding.searchView.addTextChangedListener { text ->
            viewModel.updateQuery(text?.toString() ?: "")
        }
    }

    override fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(context)
        binding.citiesList.layoutManager = layoutManager
        binding.citiesList.adapter = adapter
    }

    override fun observeObservers() {
        super.observeObservers()

        viewModel.getActionLiveData().observe(viewLifecycleOwner, { action ->
            when (action) {
                Constants.OPEN_WEATHER_INFO -> openWeatherInfoFragment(
                    viewModel.lastClickedSearchItemNameKey
                )
            }
        })
    }

    override fun afterLoadData(data: List<City>) {
        adapter.submitList(data)
    }

    override fun getViewModel(): BaseDataListViewModel<City> = viewModel

    override fun clickToListItem(model: City) {
        viewModel.storeClickedSearchItem(model)
        openWeatherInfoFragment(model.getTitle())
    }

    private fun openWeatherInfoFragment(nameKey: String?) {
        val arguments = bundleOf(Constants.NAME_BUNDLE_KEY to nameKey)
        findNavController().navigate(R.id.action_search_city_to_weather_info, arguments)
    }
}