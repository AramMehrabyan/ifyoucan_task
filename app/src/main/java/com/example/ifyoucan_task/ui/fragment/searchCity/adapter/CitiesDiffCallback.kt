package com.example.ifyoucan_task.ui.fragment.searchCity.adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.ifyoucan_task.data.model.City

class CitiesDiffCallback : DiffUtil.ItemCallback<City>() {

    override fun areItemsTheSame(
        oldItem: City, newItem: City
    ): Boolean = oldItem.name == newItem.name

    override fun areContentsTheSame(
        oldItem: City, newItem: City
    ): Boolean = oldItem.state == newItem.state &&
            oldItem.country == newItem.country
}