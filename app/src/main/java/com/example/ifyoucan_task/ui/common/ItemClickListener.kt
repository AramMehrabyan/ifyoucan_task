package com.example.ifyoucan_task.ui.common

typealias ListItemClickListener<T> = (model: T) -> Unit