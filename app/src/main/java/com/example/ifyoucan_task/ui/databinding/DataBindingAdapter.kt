package com.example.ifyoucan_task.ui.databinding

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.GenericTransitionOptions
import com.example.ifyoucan_task.R
import com.example.ifyoucan_task.util.imageLoader.GlideApp

@BindingAdapter("imgUrl", "errorImageRes", requireAll = true)
fun setImageFromUrl(imageView: ImageView, imgUrl: String?, errorImageRes: Int) {
    if (imgUrl == null) return
    GlideApp.with(imageView.context)
        .load(imgUrl)
        .imageLoadCommonRequest(errorImageRes)
        .transition(GenericTransitionOptions.with(R.anim.load_image_anim))
        .into(imageView)
}

@BindingAdapter("backgroundRes")
fun setViewBackground(view: View, backgroundRes: Int) {
    if (backgroundRes == 0) return
    view.setBackgroundResource(backgroundRes)
}

@BindingAdapter("textColorRes")
fun setTextColor(textView: TextView, textColorRes: Int) {
    if (textColorRes == 0) return
    textView.setTextColor(
        ResourcesCompat.getColor(textView.resources, textColorRes, null)
    )
}