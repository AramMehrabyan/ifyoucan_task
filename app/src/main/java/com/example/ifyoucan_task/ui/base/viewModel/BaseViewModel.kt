package com.example.ifyoucan_task.ui.base.viewModel

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.ifyoucan_task.util.ActionLiveData

abstract class BaseViewModel : ViewModel() {

    val loadingProgressVisibility: MutableLiveData<Int> by lazy {
        MutableLiveData(View.GONE)
    }

    private val errorMessageLiveData: ActionLiveData<String> by lazy {
        ActionLiveData()
    }
    protected val actionLiveData: ActionLiveData<String> by lazy {
        ActionLiveData()
    }

    fun getActionLiveData(): LiveData<String> = actionLiveData

    fun getErrorMessageLiveData(): LiveData<String> = errorMessageLiveData

    protected open fun doOnStartLoading() {
        loadingProgressVisibility.value = View.VISIBLE
    }

    protected open fun doOnEndLoading() {
        loadingProgressVisibility.value = View.GONE
    }

    protected abstract fun loadData()

    protected open fun doOnLoadingError(errorMessage: String) {
        doOnEndLoading()
        errorMessageLiveData.sendAction(errorMessage)
    }
}