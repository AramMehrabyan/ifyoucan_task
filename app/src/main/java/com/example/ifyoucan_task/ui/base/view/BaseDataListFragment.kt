package com.example.ifyoucan_task.ui.base.view

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.ifyoucan_task.ui.base.viewModel.BaseDataListViewModel

abstract class BaseDataListFragment<DATA> : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeObservers()
    }

    protected open fun observeObservers() {
        with(getViewModel()) {
            getData().observe(viewLifecycleOwner, { data ->
                afterLoadData(data)
            })
        }
    }

    protected abstract fun afterLoadData(data: List<DATA>)

    protected abstract fun getViewModel(): BaseDataListViewModel<DATA>

    protected abstract fun initRecyclerView()

    protected open fun clickToListItem(model: DATA) = Unit
}