package com.example.ifyoucan_task.ui.fragment.searchCity.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.ifyoucan_task.R
import com.example.ifyoucan_task.data.model.City
import com.example.ifyoucan_task.databinding.ItemCityBinding
import com.example.ifyoucan_task.ui.common.ListItemClickListener

class CitiesAdapter(
    private val itemClickListener: ListItemClickListener<City>
) : ListAdapter<City, CitiesAdapter.CitiesViewHolder>(
    CitiesDiffCallback()
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CitiesViewHolder {
        val binding: ItemCityBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_city,
            parent, false
        )

        return CitiesViewHolder(binding, itemClickListener)
    }

    override fun onBindViewHolder(holder: CitiesViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class CitiesViewHolder(
        private val binding: ItemCityBinding,
        itemClick: ListItemClickListener<City>
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                val model = binding.model ?: return@setOnClickListener
                itemClick(model)
            }
        }

        fun bind(model: City) {
            binding.model = model
        }
    }
}