package com.example.ifyoucan_task.ui.fragment.weatherInfo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.ifyoucan_task.databinding.FragmentWeatherInfoBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WeatherInfoFragment : Fragment() {

    private val viewModel: WeatherInfoViewModel by viewModels()
    private lateinit var binding: FragmentWeatherInfoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            viewModel.fetchDataFromArgumentsAndLoadData(it)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentWeatherInfoBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        init()
        observeObservers()

        return binding.root
    }

    private fun init() {
        binding.toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun observeObservers() {
        viewModel.getErrorMessageLiveData().observe(viewLifecycleOwner, { message ->
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        })
    }
}