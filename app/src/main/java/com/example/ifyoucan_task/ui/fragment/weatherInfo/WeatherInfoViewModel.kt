package com.example.ifyoucan_task.ui.fragment.weatherInfo

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.ifyoucan_task.R
import com.example.ifyoucan_task.data.model.Weather
import com.example.ifyoucan_task.data.remote.client.ResponseWrapper
import com.example.ifyoucan_task.domain.WeatherUseCase
import com.example.ifyoucan_task.ui.base.viewModel.BaseViewModel
import com.example.ifyoucan_task.util.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class WeatherInfoViewModel @Inject constructor(
    private val weatherUseCase: WeatherUseCase
) : BaseViewModel() {

    val errorIconRes = R.drawable.weather_error_icon
    val locationName: MutableLiveData<String> = MutableLiveData()
    val iconUrl: MutableLiveData<String> = MutableLiveData()
    val nightDayBgRes: MutableLiveData<Int> = MutableLiveData()
    val nightDayTextColorRes: MutableLiveData<Int> = MutableLiveData()
    val nightDayInfo: MutableLiveData<String> = MutableLiveData()
    val temperatureInfo: MutableLiveData<String> = MutableLiveData()
    val description: MutableLiveData<String> = MutableLiveData()
    val windSpeedInfo: MutableLiveData<String> = MutableLiveData()

    init {
        loadData()
    }

    override fun loadData() {
        val nameParam = locationName.value ?: return
        doOnStartLoading()
        viewModelScope.launch(Dispatchers.IO) {
            val response = weatherUseCase.getCityWeatherInfo(nameParam)

            withContext(Dispatchers.Main) {
                when (response) {
                    is ResponseWrapper.Success -> updateData(response.data)
                    is ResponseWrapper.Error -> doOnLoadingError(response.message)
                }
            }
        }
    }

    private fun updateData(data: Weather) {
        doOnEndLoading()
        data.weatherInfo?.firstOrNull()?.let { weatherInfo ->
            configureDayNightResources(weatherInfo.icon)
            description.value = weatherInfo.description ?: ""
        }

        data.mainInfo?.let {
            temperatureInfo.value = "${it.temp.toInt()} ${Constants.TEMPERATURE_CELSIUS}"
        }

        data.wind?.let {
            windSpeedInfo.value = it.speed.toString()
        }
    }

    private fun configureDayNightResources(icon: String?) {
        if (icon?.endsWith(Constants.CURRENT_TIME_TYPE_SUFFIX) == true) {
            nightDayInfo.value = Constants.CURRENT_TIME_TYPE_DAY
            nightDayBgRes.value = R.color.weather_info_bg_day
            nightDayTextColorRes.value = R.color.weather_info_text_color_day
        } else {
            nightDayInfo.value = Constants.CURRENT_TIME_TYPE_NIGHT
            nightDayBgRes.value = R.color.weather_info_bg_night
            nightDayTextColorRes.value = R.color.weather_info_text_color_night
        }

        icon?.let {
            iconUrl.value =
                "${Constants.WEATHER_IMAGE_URL_START}$it${Constants.WEATHER_IMAGE_URL_END}"
        }
    }

    fun fetchDataFromArgumentsAndLoadData(arguments: Bundle) {
        locationName.value = arguments.getString(Constants.NAME_BUNDLE_KEY)
        loadData()
    }
}