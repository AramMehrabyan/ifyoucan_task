package com.example.ifyoucan_task.ui.base.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

abstract class BaseDataListViewModel<DATA> : BaseViewModel() {

    private val data: MutableLiveData<List<DATA>> by lazy {
        MutableLiveData()
    }

    fun getData(): LiveData<List<DATA>> = data

    protected fun updateData(newData: List<DATA>) {
        data.value = newData
        doOnEndLoading()
    }
}