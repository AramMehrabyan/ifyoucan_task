package com.example.ifyoucan_task.ui.fragment.searchCity

import androidx.lifecycle.viewModelScope
import com.example.ifyoucan_task.data.model.City
import com.example.ifyoucan_task.data.remote.client.ResponseWrapper
import com.example.ifyoucan_task.domain.WeatherUseCase
import com.example.ifyoucan_task.ui.base.viewModel.BaseDataListViewModel
import com.example.ifyoucan_task.util.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@FlowPreview
@HiltViewModel
class SearchCityViewModel @Inject constructor(
    private val weatherUseCase: WeatherUseCase
) : BaseDataListViewModel<City>() {

    private val searchQuery = MutableStateFlow("")
    var lastClickedSearchItemNameKey: String? = null

    init {
        initSearchQuery()
        initLastClickedSearchItemFlow()
    }

    private fun initSearchQuery() {
        searchQuery.debounce(Constants.SEARCH_DEBOUNCE_DURATION_MS)
            .flowOn(Dispatchers.Default)
            .filter { query ->
                return@filter query.isNotEmpty()
            }.onEach {
                loadData()
            }.launchIn(viewModelScope)
    }

    private fun initLastClickedSearchItemFlow() {
        viewModelScope.launch(Dispatchers.IO) {
            val model: City = weatherUseCase.getLastSearchedCity() ?: return@launch
            withContext(Dispatchers.Main) {
                handleLastClickedSearchItem(model)
            }
        }
    }

    private fun handleLastClickedSearchItem(model: City) {
        lastClickedSearchItemNameKey = model.getTitle()
        actionLiveData.value = Constants.OPEN_WEATHER_INFO
    }

    fun updateQuery(key: String) {
        searchQuery.value = key
    }

    override fun loadData() {
        doOnStartLoading()
        viewModelScope.launch(Dispatchers.IO) {
            val response = weatherUseCase.getCitiesList(searchQuery.value)

            withContext(Dispatchers.Main) {
                when (response) {
                    is ResponseWrapper.Success -> updateData(response.data)
                    is ResponseWrapper.Error -> updateData(emptyList())
                }
            }
        }
    }

    fun storeClickedSearchItem(model: City) {
        viewModelScope.launch(Dispatchers.IO) {
            weatherUseCase.insertLastSearchedCity(model)
        }
    }
}