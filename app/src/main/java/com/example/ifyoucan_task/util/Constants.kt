package com.example.ifyoucan_task.util

object Constants {

    const val WEATHER_API_URL = "https://api.openweathermap.org"
    const val LAST_SEARCHED_CITY_MODEL_ID = 1
    const val REQUEST_CONNECTION_TIMEOUT: Long = 30
    const val REQUEST_READ_TIMEOUT: Long = 30
    const val SEARCH_DEBOUNCE_DURATION_MS: Long = 500
    const val CELSIUS_UNIT = "metric"
    const val WEATHER_DB_NAME = "weather_db"
    const val WEATHER_IMAGE_URL_START = "https://openweathermap.org/img/wn/"
    const val WEATHER_IMAGE_URL_END = "@2x.png"
    const val TEMPERATURE_CELSIUS = "C"

    const val CURRENT_TIME_TYPE_NIGHT = "Night"
    const val CURRENT_TIME_TYPE_DAY = "Day"
    const val CURRENT_TIME_TYPE_SUFFIX = "d"

    // Bundle keys
    const val NAME_BUNDLE_KEY = "name"

    // Actions
    const val OPEN_WEATHER_INFO = "open_weather_info"

    //Message
    const val COMMON_ERROR_MESSAGE = "Something went wrong!"
}