package com.example.ifyoucan_task.util.imageLoader

import com.bumptech.glide.annotation.GlideExtension
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.annotation.GlideOption
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.BaseRequestOptions

@GlideModule
class GlideModule : AppGlideModule()

@GlideExtension
class GlideExtension private constructor() {

    companion object {

        @GlideOption
        @JvmStatic
        fun imageLoadCommonRequest(
            options: BaseRequestOptions<*>,
            errorImageRes: Int
        ): BaseRequestOptions<*> = options
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .centerCrop()
            .fallback(errorImageRes)
            .error(errorImageRes)
            .transform(
                MultiTransformation(CenterCrop())
            )
    }
}